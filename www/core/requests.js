angular
    .module('http.module', [])
    .factory('http', [
        '$http',
        '$sessionStorage',
        '$q',
        '$rootScope',
        function ($http, $sessionStorage, $q, $rootScope) {
            return {
                get: function (url, data) {
                    return request('GET', url, data);
                },
                post: function (url, data) {
                    return request('POST', url, data);
                },
                delete: function (url, data) {
                    return request('DELETE', url, data);
                },
                put: function (url, data) {
                    return request('PUT', url, data);
                },
                file: function (url, data) {
                    return requestFile(url, data);
                },
                fileTransfer: function (status, url, fileName, path, data) {
                    return requestFileTransfer(status, url, fileName, path, data);
                }
            };

            function requestFileTransfer(status, url, fileName, path, data) {
                $rootScope.loading = true;
                if (typeof cordova !== 'undefined') {
                    var ft = new FileTransfer(),
                        deferred = $q.defer(),
                        downloadPath,
                        rootFolder,
                        requestUrl;

                    if ($sessionStorage.auth_key) {
                        requestUrl = url + '?auth_key=' + $sessionStorage.auth_key;
                    }
                    else {
                        requestUrl = url;
                    }


                    if (status === 'upload') {
                        console.log(data);
                        ft.upload(path, encodeURI(requestUrl), fileSuccess(deferred), fileError(deferred),
                            {
                                fileName: fileName,
                                fileKey: 'file',
                                mimeType: 'image/jpeg',
                                httpMethod: 'POST',
                                chunkedMode: false,
                                params: data
                            }
                        );
                    }

                    if (status === 'download') {
                        rootFolder = cordova.file.externalRootDirectory;
                        downloadPath = rootFolder + 'Download/' + fileName;

                        ft.download(encodeURI(requestUrl), downloadPath, fileSuccess(deferred), fileError(deferred), false,
                            {params: data}
                        );
                    }

                    $rootScope.loading = false;

                    return deferred.promise;
                } else {
                    console.log('cordova is not defined');
                }
            }

            function fileSuccess(def) {
                return function (result) {
                    console.log(result);
                    $rootScope.loading = false;
                    def.resolve(result);
                };
            }

            function fileError(def) {
                return function (error) {
                    console.log(error);
                    $rootScope.loading = false;
                    def.reject(error);
                };
            }

            function request(method, url, data) {
                $rootScope.loading = true;

                var config = {
                    dataType: 'json',
                    method: method,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json; charset=UTF-8'
                    }
                };

                if (method === 'GET') {
                    config.params = data;
                }
                else {
                    config.data = data;
                }

                if ($sessionStorage.auth_key) {
                    config.url = url + '?auth_key=' + $sessionStorage.auth_key;
                }
                else {
                    config.url = url;
                }

                return $http(config)
                    .then(requestComplete)
                    .catch(requestFailed);
            }

            function requestFile(url, data) {
                $rootScope.loading = true;

                var config = {
                    transformRequest: angular.identity,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                if ($sessionStorage.auth_key) {
                    url = url + '?auth_key=' + $sessionStorage.auth_key;
                }

                return $http.post(url, data, config)
                    .then(requestComplete)
                    .catch(requestFailed);
            }

            function requestFailed(err) {
                console.info('error', err.config.url, err);

                if (err.data === null || !err.data.error) {
                    if (err.status === 200) {
                        window.plugins.toast.show('Server Error: ' + err.data, 'long', 'center');
                    }
                    else if (err.status === 0) {
                        window.plugins.toast.show('Server unavailable', 'long', 'center');
                    }
                    else if (err.status === -1) {
                        window.plugins.toast.show('No internet connection', 'long', 'center');
                    }
                    else if (err.status === 500) {
                        window.plugins.toast.show('Server Error: ' + err.status + ' ' + err.data.message, 'long', 'center');
                    }
                    else {
                        window.plugins.toast.show('Server Error: ' + err.status + ' ' + err.statusText, 'long', 'center');
                    }
                }
                else {
                    window.plugins.toast.show('Error: ' + err.data.error, 'long', 'center');
                }

                $rootScope.loading = false;

                return $q.reject(err.data.error);
            }

            function requestComplete(response) {
                var promise = $q.defer();

                console.info('response complete', response.config.url + '\n', response.data);

                if (!response.data.error) {
                    promise.resolve(response.data);
                }
                else {
                    promise.reject(response);
                }

                $rootScope.loading = false;

                return promise.promise;
            }
        }]
    );