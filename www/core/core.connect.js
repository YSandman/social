angular
    .module('core', [
        'ngStorage',

        'url.module',
        'http.module',
        'constants.module',
        'factories.module',
        'services.module',
        'directives.module'
    ]);