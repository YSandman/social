angular
    .module('url.module', [])
    .factory('url', function () {
        var baseUrl = 'http://5s.grassbusinesslabs.com/server/api/web/v1/';
        // var baseUrl = 'http://192.168.0.172/api/web/v1/';

        return {
            login: baseUrl + 'user/login',
            getDepartments: baseUrl + 'department/index',
            getPlaces: baseUrl + 'place/index',
            getPoints: baseUrl + 'place-audit/index',
            setPoint: baseUrl + 'place-audit/update'
        };
    });