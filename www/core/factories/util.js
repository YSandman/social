factories
        .factory('util', util);

    function util() {

        return {
            isValidPassword: isValidPassword,
            emptyItems: emptyItems,
            isValidCountSymbol: isValidCountSymbol
        };

        function isValidPassword(password) {
            return (password.length >= 6)
        }

        function emptyItems(items) {
            var res = false;
            angular.forEach(items, function (item) {
                if (!item || typeof item == 'string' && item.trim().length == 0) {
                    res = true;
                }
            });
            return res;
        }

        function isValidCountSymbol(field, count) {
            return field.length <= count;
        }
    }
