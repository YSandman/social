services
    .service('user', user);

user.$inject = ['http', 'url', '$sessionStorage'];

function user(http, url, $sessionStorage) {

    return {
        login: login,

        getDepartments: getDepartments,
        getPlaces: getPlaces,
        getCheckList: getCheckList,
        getPoints: getPoints,
        setPoint: setPoint,
        setPointWithImg: setPointWithImg,

        logout: logout
    };

    this.state = '';

    function login(data, callback) {
        http.post(url.login, data)
            .then(function (response) {
                if (!response.status) {
                    window.plugins.toast.show('Wrong data', 'long', 'center');
                }
                else {
                    $sessionStorage.auth_key = response.auth_key;
                    callback(response);
                }
            });
    }

    function getDepartments(data, callback) {
        http.get(url.getDepartments, data)
            .then(function (response) {
                callback(response);
            });
    }

    function getPlaces(data, callback) {
        http.get(url.getPlaces, data)
            .then(function (response) {
                callback(response);
            });
    }

    function getCheckList(data, callback) {
        http.get(url.getCheckList, data)
            .then(function (response) {
                callback(response);
            });
    }

    function getPoints(data, callback) {
        http.get(url.getPoints, data)
            .then(function (response) {
                callback(response);
            });
    }

    function setPoint(data, callback) {
        http.post(url.setPoint, data)
            .then(function (response) {
                callback(response);
            });
    }

    function setPointWithImg(data, callback) {
        http.fileTransfer(
            'upload',
            url.setPoint,
            data.id,
            data.file,
            data)
            .then(function (response) {
                callback(response);
            });
    }

    function logout() {
        delete $sessionStorage.auth_key;
    }
}