var social = angular.module('social', [
    'ionic',
    'ngMaterial',
    'ngAnimate',
    'ngAria',
    'ngStorage',
    'pascalprecht.translate',
    'angular-loading-bar',

    'core'
])
    .run([
        '$rootScope',
        '$state',
        '$sessionStorage',
        '$ionicHistory',
        function ($rootScope, $state, $sessionStorage, $ionicHistory) {
            document.addEventListener('deviceready', onOnline, false);

            function onOnline() {
                if ($sessionStorage.auth_key) {
                    $rootScope.globalTranslate = 'en';
                    user.get()
                        .then(function (response) {
                            if (!response.active) {
                                window.plugins.toast.show('Please check your inbox to verify your email for Propeller', 'long', 'center');
                                return false;
                            } else {
                                $rootScope.currentItem = null;
                                $rootScope.user = response;
                                $ionicHistory.nextViewOptions({
                                    disableBack: true
                                });

                                $state.go('app.workplacesList');
                            }
                        }, function (err) {
                            console.log(err);
                            $state.go('login');
                        });
                } else {
                    $state.go('login');
                }
            }
        }])
    .config(function ($ionicConfigProvider, $ionicLoadingConfig, $translateProvider, cfpLoadingBarProvider, $mdGestureProvider) {
        $ionicConfigProvider.views.maxCache(0);
        $ionicConfigProvider.views.swipeBackEnabled(false);
        $ionicConfigProvider.backButton.text('');
        $mdGestureProvider.skipClickHijack();
        cfpLoadingBarProvider.includeSpinner = false;

        /**
         * Configuring ionic loader
         */
        angular.extend($ionicLoadingConfig, {
            noBackdrop: true
        });

        $translateProvider
            .useStaticFilesLoader({
                prefix: 'locales/',
                suffix: '.json'
            })
            .registerAvailableLanguageKeys(['ua', 'en'], {
                'en': 'en', 'en_GB': 'en', 'en_US': 'en',
                'de': 'de', 'de_DE': 'de', 'de_CH': 'de'
            })
            .preferredLanguage('en')
            .fallbackLanguage('en')
            .determinePreferredLanguage()
            .useSanitizeValueStrategy('escapeParameters');
    });