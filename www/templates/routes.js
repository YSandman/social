social
    .config([
        '$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/login');

            $stateProvider
                .state('login', {
                    url: '/login',
                    templateUrl: 'templates/login/login.html',
                    title: 'Login',
                    controller: 'Login',
                    controllerAs: 'vm'
                })
                .state('signup', {
                    url: '/signup',
                    templateUrl: 'templates/signup/signup.html',
                    title: 'signup',
                    controller: 'Signup',
                    controllerAs: 'vm'
                })
                .state('app', {
                    url: '/app',
                    templateUrl: 'templates/header/header.html',
                    controller: 'Header',
                    controllerAs: 'vm'
                })
                .state('app.departmentsList', {
                    url: '/departmentsList',
                    templateUrl: 'templates/departmentsList/departmentsList.html',
                    title: 'DepartmentsList',
                    controller: 'Departments',
                    controllerAs: 'vm'
                })
                .state('app.workplacesList', {
                    url: '/workplacesList/:department_id',
                    templateUrl: 'templates/workplacesList/workplacesList.html',
                    title: 'WorkplacesList',
                    controller: 'Workplaces',
                    controllerAs: 'vm'
                })
                .state('app.checksList', {
                    url: '/checksList/:place_id',
                    templateUrl: 'templates/checksList/checksList.html',
                    title: 'checksList',
                    controller: 'Checks',
                    controllerAs: 'vm'
                })
                .state('app.criteriaList', {
                    url: '/criteriaList/:place_id',
                    templateUrl: 'templates/criteriaList/criteriaList.html',
                    title: 'CriteriaList',
                    controller: 'criteriaListCtrl',
                    controllerAs: 'vm'
                })
                .state('app.moreInfo', {
                    url: '/info',
                    templateUrl: 'templates/moreInfo/moreInfo.html',
                    title: 'MoreInfo',
                    controller: 'moreInfoCtrl',
                    controllerAs: 'vm',
                    params: {
                        item: null
                    }
                });
        }]
    );