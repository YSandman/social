social
    .controller('Workplaces', [
        '$ionicHistory',
        '$scope',
        'user',
        '$stateParams',
        '$state',
        function ($ionicHistory, $scope, user, $stateParams, $state) {
            var vm = this;
            vm.items = [];

            // console.log(user.state);

            user.getPlaces({department_id: $stateParams.department_id}, function (data) {
                vm.items = data;

                if (data.length == 1 && user.state != 'app.criteriaList') {
                    $scope.$root.stakeTitle = $scope.$root.title;
                    $scope.$root.title = data[0].name;
                    $state.go("app.criteriaList", {place_id: data[0].id});
                }
            });
        }]
    );