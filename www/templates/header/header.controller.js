social
    .controller('Header', [
        '$state',
        '$scope',
        '$ionicHistory',
        'user',
        '$ionicPopup',
        '$translate',
        function ($state, $scope, $ionicHistory, user, $ionicPopup, $translate) {
            vm = this;

            $scope.clickBack = function () {
                $scope.$root.title = $scope.$root.stakeTitle;
                $ionicHistory.goBack();
            };

            $scope.openMenu = function ($mdMenu, ev) {
                $mdMenu.open(ev);
            };

            $scope.menuLogout = function () {
                user.logout();
                $state.go('login');
            };

            $scope.setLanguage = function () {
                $translate('MENU_DIALOG_SWITCH_LANGUAGE').then(function (text) {
                    vm.dialogHeader = text;
                    $translate('CRITERIA_DESCRIPTION_DIALOG_CANCEL').then(function (text) {
                        vm.dialogCancel = text;
                        $translate('CRITERIA_DESCRIPTION_DIALOG_OK').then(function (text) {
                            vm.dialogSave = text;

                            $ionicPopup.show({
                                templateUrl: 'templates/translates.html',
                                title: vm.dialogHeader,
                                scope: $scope,

                                buttons: [
                                    {
                                        text: vm.dialogCancel
                                    },
                                    {
                                        text: vm.dialogSave,
                                        type: 'button-positive',
                                        onTap: function (e) {
                                            $translate.use($scope.$root.globalTranslate);
                                        }
                                    }
                                ]
                            });
                        });
                    });
                });
            };

            $scope.$root.languagesList = {
                en: {text: 'English', src: 'img/england.png'},
                ua: {text: 'Ukrainian', src: 'img/ua.png'}
            };
        }]
    );