social
    .controller('criteriaListCtrl', [
        '$state',
        '$scope',
        '$stateParams',
        'user',
        '$ionicPopup',
        '$sessionStorage',
        '$translate',
        '$ionicHistory',
        function ($state, $scope, $stateParams, user, $ionicPopup, $sessionStorage, $translate, $ionicHistory) {
            var vm = this;

            vm.groups = [];
            vm.setPoint = setPoint;
            vm.openDescriptionDialog = openDescriptionDialog;
            vm.openMenu = openMenu;

            user.getPoints({place_id: $stateParams.place_id}, function (data) {
                vm.groups = data;
            });

            vm.toggleGroup = function (item) {
                if (vm.isGroupShown(item)) {
                    vm.shownGroup = null;
                } else {
                    vm.shownGroup = item;
                }
            };

            vm.isGroupShown = function (item) {
                return vm.shownGroup === item;
            };

            vm.openCamera = openCamera;
            vm.removeImage = removeImage;

            function openMenu($mdMenu, ev) {
                $mdMenu.open(ev);
            }

            function openDescriptionDialog(index) {
                vm.data = vm.groups[index].description;
                $translate('CRITERIA_DESCRIPTION_DIALOG_HEADER').then(function (text) {
                    vm.dialogHeader = text;
                    $translate('CRITERIA_DESCRIPTION_DIALOG_CANCEL').then(function (text) {
                        vm.dialogCancel = text;
                        $translate('CRITERIA_DESCRIPTION_DIALOG_OK').then(function (text) {
                            vm.dialogSave = text;

                            var popup = $ionicPopup.show({
                                template: '<textarea autofocus type="text" rows="5" ng-model="vm.data" style="resize: vertical;">' +
                                '</textarea>',
                                title: vm.groups[index].name,
                                subTitle: vm.dialogHeader,
                                scope: $scope,
                                buttons: [
                                    {
                                        text: vm.dialogCancel,
                                        onTap: function (e) {
                                            return false;
                                        }
                                    },
                                    {
                                        text: vm.dialogSave,
                                        type: 'button-positive',
                                        onTap: function (e) {
                                            return vm.data;
                                        }
                                    }
                                ]
                            });

                            popup.then(function (res) {
                                if (res) {
                                    vm.groups[index].description = res;
                                }
                            });
                        });
                    });
                });
            }

            function setOptions(srcType) {
                return {
                    // Some common settings are 20, 50, and 100
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: srcType,
                    encodingType: Camera.EncodingType.JPEG,
                    mediaType: Camera.MediaType.PICTURE,
                    allowEdit: false,
                    correctOrientation: true
                };
            }

            function openCamera(index, type) {
                var srcType = type == 1 ?
                    Camera.PictureSourceType.CAMERA :
                    navigator.camera.PictureSourceType.PHOTOLIBRARY;
                var options = setOptions(srcType);

                navigator.camera.getPicture(function cameraSuccess(imageUri) {
                    displayImage(index, imageUri);
                }, function cameraError(error) {
                    console.debug("Unable to obtain picture: " + error, "app");
                }, options);
            }

            function displayImage(index, imgUri) {
                console.log(imgUri);
                $scope.safeApply(function () {
                    vm.groups[index].img = imgUri;
                });
            }

            function removeImage(index) {
                $scope.safeApply(function () {
                    vm.groups[index].img = '';
                });
            }

            function setPoint(item, mark, index) {
                if(item.img == '') {
                    user.setPoint({
                        auth_key: $sessionStorage.auth_key,
                        point_id: item.id,
                        file: '',
                        description: item.description,
                        status: mark
                    }, function (data) {
                        if (data.status) {
                            slicePoint(index);
                        }
                    })
                }
                else {
                    user.setPointWithImg({
                        auth_key: $sessionStorage.auth_key,
                        point_id: item.id,
                        file: item.img,
                        description: item.description,
                        status: mark
                    }, function (data) {
                        console.log(data);
                        if (data.responseCode == 200) {
                            slicePoint(index);
                        }
                    });
                }

                vm.groups.length == index + 1 ? vm.toggleGroup(vm.groups[0]) : vm.toggleGroup(vm.groups[index + 1]);
            }

            function slicePoint(index) {
                vm.groups.splice(index, 1);
                if (vm.groups.length == 0) {
                    $ionicHistory.goBack();
                    // $state.go("app.checksList", {place_id: nope});
                }
            }

            $scope.safeApply = function (fn) {
                var phase = this.$root.$$phase;
                if (phase == '$apply' || phase == '$digest') {
                    if (fn && (typeof(fn) === 'function')) {
                        fn();
                    }
                } else {
                    this.$apply(fn);
                }
            };

            $scope.$root.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    user.state = fromState.name;
                })
        }]
    );