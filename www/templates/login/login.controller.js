social
    .controller('Login', [
        'http',
        'url',
        '$sessionStorage',
        '$state',
        '$ionicPopup',
        '$scope',
        '$ionicScrollDelegate',
        '$ionicHistory',
        'user',
        function (http, url, $sessionStorage, $state, $ionicPopup, $scope, $ionicScrollDelegate, $ionicHistory, user) {
            var vm = this;

            vm.login = login;
            vm.scroll = scroll;
            vm.forgotPassword = forgotPassword;
            vm.data = {
                email: 'admin@ukr.net',
                password: '111111'
            };

            /**
             * Function for send data to server
             * and login user
             */
            function login() {
                if (!vm.data.email) {
                    window.plugins.toast.show('Invalid email ID. Please use an email ID with an approved domain name', 'long', 'center');
                }
                else if (!vm.data.password) {
                    window.plugins.toast.show('You enter empty password', 'long', 'center');
                }
                else {
                    user.login(vm.data, function (response) {
                        if (response.status) {
                            $ionicHistory.nextViewOptions({
                                disableBack: true
                            });
                            $state.go('app.departmentsList', {}, {location: 'replace'});
                        }
                    });
                }
            }

            /**
             * Function for scroll to bottom page
             */
            function scroll() {
                $ionicScrollDelegate.scrollBottom(true);
            }

            /**
             * Function for show popup to forgot password
             */
            function forgotPassword() {
                $ionicPopup.show({
                    template: '<input type="text" maxlength="50" style="padding-left: 10px; padding-right: 10px" ng-model="vm.data.email">',
                    title: 'Enter registered email address',
                    scope: $scope,
                    buttons: [
                        {text: 'Cancel'},
                        {
                            text: '<b>Send</b>',
                            type: 'button-positive',
                            onTap: function (e) {
                                if (!vm.data.email) {
                                    window.plugins.toast.show('Invalid email ID. Please use an email ID with an approved domain name', 'long', 'center');
                                    return;
                                }
                                user.restore(vm.data);
                                delete vm.data;
                            }
                        }
                    ]
                });
            }
        }]
    );