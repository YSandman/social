social
    .controller('Checks', [
        '$ionicHistory',
        '$scope',
        '$stateParams',
        'user',
        '$state',
        function ($ionicHistory, $scope, $stateParams, user, $state) {
            var vm = this;

            vm.checks = [];

            user.getCheckList({place_id: $stateParams.place_id}, function (data) {
                vm.checks = data;
                if (data.length == 1) {
                    $state.go("app.criteriaList", {list_id: data[0].id});
                }
            });
        }]
    );