social
    .controller('Departments', [
        'user',
        '$state',
        function (user, $state) {
            var vm = this;
            vm.items = [];

            user.getDepartments({}, function (data) {
                vm.items = data;
                if (data.length == 1) {
                    $state.go("app.workplacesList", {department_id: data[0].id});
                }
            });
        }]
    );