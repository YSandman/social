social
    .controller('Signup', [
        'http',
        'url',
        '$sessionStorage',
        '$state',
        '$ionicHistory',
        function (http, url, $sessionStorage, $state, $ionicHistory) {
            var vm = this;
            vm.signup = signup;
            vm.back = back;
            vm.data = {
                username: '',
                email: '',
                password: '',
                rpassword: ''
            };

            /**
             * Function for validation signup data
             * and send data to server
             */
            function signup() {
                if(util.emptyItems(vm.data)){
                    window.plugins.toast.show('Please complete all fields', 'long', 'center');
                    return;
                }else if(vm.data.password != vm.data.rpassword) {
                    window.plugins.toast.show('Passwords don\'t match', 'long', 'center');
                    return;
                }else if(!util.isValidPassword(vm.data.password)){
                    window.plugins.toast.show('Password should be at least six characters', 'long', 'center');
                    return;
                }

                user.signup(vm.data)
                    .then(function () {
                        window.plugins.toast.show('Please check your inbox to verify your email for Propeller', 'long', 'center');
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go('login', {}, {location: 'replace'});
                    })
            }

            /**
             * Function for back button
             */
            function back() {
                $ionicHistory.goBack();
            }
        }]
    );