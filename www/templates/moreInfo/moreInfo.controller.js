social
    .controller('moreInfoCtrl', [
        '$ionicHistory',
        '$scope',
        '$stateParams',
        'user',
        '$ionicModal',
        '$ionicSlideBoxDelegate',
        function ($ionicHistory, $scope, $stateParams, user, $ionicModal, $ionicSlideBoxDelegate) {
            var vm = this;

            vm.data = $scope.$root.currentItem = $stateParams.item;
            console.log(vm.data);
            vm.testData = [
                {
                    url: 'http://myprava.ge/Newprava/17.jpg',
                    description: 'This will green',
                    mark: 'green',
                    date: new Date(),
                    id: 0
                },
                {
                    url: 'http://beebom.com/wp-content/uploads/2016/01/Reverse-Image-Search-Engines-Apps-And-Its-Uses-2016.jpg',
                    description: 'Some dude here Some dude here Some dude here Some dude here Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here  Some dude here ',
                    mark: 'yellow',
                    date: new Date(),
                    id: 1
                },
                {
                    url: 'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg',
                    description: 'https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg https://www.smashingmagazine.com/wp-content/uploads/2015/06/10-dithering-opt.jpg',
                    mark: 'red',
                    date: new Date(),
                    id: 2
                }
            ];

            vm.showImage = showImage;
            vm.closeModal = closeModal;
            vm.next = next;
            vm.previous = previous;

            function showImage(images) {
                $ionicModal.fromTemplateUrl('templates/moreInfo/image_popover/image.html', {
                    scope: $scope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    vm.modalImage = modal;
                    vm.modalImage.images = vm.testData;
                    vm.modalImage.show();
                });
            }

            function closeModal() {
                vm.modalImage.hide();
            }

            function next() {
                $ionicSlideBoxDelegate.next();
            }

            function previous() {
                $ionicSlideBoxDelegate.previous();
            }
        }]
    );