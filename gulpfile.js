var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    concatCss = require('gulp-concat-css'),
    autoprefixer = require('gulp-autoprefixer');

// Css
gulp.task('css', function () {
    return gulp.src([
        'scss/*.scss'
    ]) // 'css/*.css'
        .pipe(plumber({
            errorHandler: function (err) {  //errorHandler  handleError
                console.log(err);
                this.emit('end');
            }
        }))
        .pipe(sass())
        .pipe(concatCss('main.css'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('www/css'));
});

// Watch
gulp.task('watch', function () {
    gulp.watch([

        'scss/*.scss'

    ], ['css']);
});

//default
gulp.task('default', ['css', 'watch']);